<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', __DIR__ . '/../');


require_once ROOT . '/app/components/Router.php';
require_once ROOT . '/app/components/Helper.php';

$router = new Router(require_once ROOT . '/routes/web.php');

$router->boot();