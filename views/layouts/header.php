<header>
    <section class="left">
        <h1>Мой магазин</h1>
    </section>
    <section class="center">
        <nav>
            <ul>
                <li><a href="<?=Helper::route('');?>">Главная</a></li>
                <li><a href="<?=Helper::route('products');?>">Продукты</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </section>
    <section class="right">
        <nav>
            <ul>
                <li><a href="#">Войти</a></li>
                <li><a href="#">Зарегистрироваться</a></li>
            </ul>
        </nav>
    </section>
</header>