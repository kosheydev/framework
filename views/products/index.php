<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../resources/css/main.css">
    <link rel="stylesheet" href="../resources/css/products.css">
    <title><?=$params['title']?></title>
</head>
<body>
    <?=Helper::layout('header');?>
    <div class="Application">
        <div class="products">
            <?php foreach($params['products'] as $product): ?>
            <div class="product-card">
                <div class="product-header">
                    <h3><?=$product['name']?></h3>
                </div>
                <div class="product-description">
                    <p><?=$product['description']?></p>
                </div>
                <div class="product-footer">
                    <div class="left">
                        <?= $product['price'] ?> &euro;
                    </div>
                    <div class="center">
                        <button>Купить</button>
                    </div>
                    <div class="right">

                    </div>

                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>
</html>