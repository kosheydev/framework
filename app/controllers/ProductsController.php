<?php

class ProductsController
{
    public function index() {
        Helper::view('products/index', [
            'title' => 'Продукция',
            'products' => [
                [
                    'id' => 1,
                    'name' => 'Продукт 1',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 2,
                    'name' => 'Продукт 2',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 3,
                    'name' => 'Продукт 3',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 4,
                    'name' => 'Продукт 4',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 5,
                    'name' => 'Продукт 5',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 6,
                    'name' => 'Продукт 6',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 7,
                    'name' => 'Продукт 7',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
                [
                    'id' => 8,
                    'name' => 'Продукт 8',
                    'description' => Helper::loremIpsum(),
                    'price' => 3500
                ],
            ]
        ]);
    }
}