<?php

class Helper
{
    public static function dump($param) {
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }

    public static function dd($param) {
        self::dump($param);
        die;
    }

    public static function view(string $view, array $params = []) {
        require_once ROOT . '/views/' . $view . '.php';
    }

    public static function route(string $route) {
        return  '/me/Oleg/public/' . $route;
    }

    public static function layout(string $layout, array $params = []) {
        require_once ROOT . '/views/layouts/' . $layout . '.php';
    }

    public static function loremIpsum(int $countParagraph): string {
        if ($countParagraph < 0) {
            return '';
        }

        $lopemIpsum = '';

        for ($i = 0; $i < $countParagraph; $i++) {
            $lopemIpsum .= 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.';
        }

        return $lopemIpsum;
    }
}