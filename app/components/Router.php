<?php

class Router
{

    const PATH = ROOT . '/app/controllers/';

    private $routes;

    public function __construct($routes) {
        $this->routes = $routes;
    }

    private function getURI() {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return substr(trim($_SERVER['REQUEST_URI'], '/'), 15);
        }
    }

    public function boot() {
        $uri = $this->getURI();

        foreach ($this->routes as $path => $handler) {
            if (preg_match("~$path~", $uri)) {
                $segments = explode('@', $handler);
                $controller = ucfirst(array_shift($segments));
                $action = array_shift($segments);

                $parameters = $segments;
                Helper::dump($parameters);

                $file = self::PATH . $controller . '.php';

                if (file_exists($file)) {
                    include_once $file;
                    $controllerObject = new $controller;
                    $controllerObject->$action();
                }

                break;
            }
        }
    }

}