<?php

return [
    'products/([0-9]+)' => 'ProductsController@show@$1',
    'products' => 'ProductsController@index',
    '' => 'HomeController@index',
];